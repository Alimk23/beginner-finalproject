<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $guarded = [];

    public function questionnaire(){
        return $this->belongsTo(Questionnaire::class);
    }
    public function answer(){
        return $this->hasMany(Answer::class);
    }

    public function rensponses(){
        return $this->hasMany(SurveyRensponse::class);
    }
}
