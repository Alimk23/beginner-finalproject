<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Survey extends Model
{
    protected $guarded = [];
    public function questionnaire(){
        return $this->belongsTo(Questionnaire::class);
    }
    
    public function rensponses(){
        return $this->hasMany(SurveyRensponse::class);
    }
}
