<?php

namespace App\Http\Controllers;

use App\Models\Question;
use Illuminate\Http\Request;
use \App\Models\Questionnaire;

class SurveysController extends Controller
{
    public function show(Questionnaire $questionnaire, $slug){
        $questionnaire->load('questions.answer');
        return view('surveys.show', compact('questionnaire'));
    }
    
    public function store(Questionnaire $questionnaire, $slug){
        // dd(request()->all());
        $data = request()->validate([
            'rensponses.*.answer_id' => 'required',
            'rensponses.*.question_id' => 'required',
            'survey.name' => 'required',
            'survey.email' => 'required|email'
        ]);

        $survey = $questionnaire->surveys()->create($data['survey']);
        $survey->rensponses()->createMany($data['rensponses']);

        return 'thanks you';
    }
}
